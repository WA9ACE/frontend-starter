let MainView = Backbone.View.extend({
  el: 'main',

  template: _.template(`
    <span>Hello <%= name %></span>

    <p>
      This is kinda like JSX right?
    </p>
  `),

  initialize: function () {
    this.render()
  },

  render: function () {
    this.$el.html(`rendered main view with ${this.template({name: 'albritton'})}`)
  }
})

export default MainView
