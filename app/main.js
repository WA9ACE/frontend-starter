import Tests from '/tests/test.js'
import MainView from '/app/views/main_view.js'

window.addEventListener('DOMContentLoaded', async () => {
  await main()

  // Run Tests by appending ?test=true to the url
  let shouldRunTests = new URLSearchParams(window.location.search).get('test')

  if (shouldRunTests) {
    await Tests.runAll()
  }
})

async function main() {
  console.log('in main')

  let mainView = new MainView()
  mainView.render()
}
