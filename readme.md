# Frontend Starter without Tooling

This repo shows an example application layout with automatic light and dark
modes using the new [@media (prefers-color-scheme)](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-color-scheme)
rule. It also makes use of ES6 Imports and CSS variables.

## Vendored CSS

* PureCSS
* FontAwesome
* Fira Code Font

## Vendored JS
* Underscore: JS helpers, required for Backbone
* Backbone: Old and stable MVC style JS framework
* ChartJS: For rendering Charts
* Moment: DateTime library because DateTime things suck by hand
* Zepto: A jQuery-like lightweight replacement library

## Screenshots
![Light Mode](/screenshots/light_mode.png)
![Dark Mode](/screenshots/dark_mode.png)
