import homeTest from '/tests/routes/test_home.js'

class Tests {
  static async runAll() {
    homeTest()
  }
}

export default Tests
